<?php
/**
* ROUTE
*/

$routesArray[] = array(
    'pattern' => '~^microsite/([a-zA-Z0-9_\-]{1,150})~',
    'controller' => 'microsite',
    'action' => 'index',
);

// Test
$routesArray[] = array(
    'pattern' => '~^test~',
    'controller' => 'page',
    'action' => 'test',
);

$routesArray[] = array(
    'pattern' => '~^login~',
    'controller' => 'page',
    'action' => 'login',
);
$routesArray[] = array(
    'pattern' => '~^profile~',
    'controller' => 'page',
    'action' => 'profile',
);

// Blog
$routesArray[] = array(
    'pattern' => '~^blog/([A-Za-z0-9\+\-\.\,_%]{1,150})~',
    'controller' => 'blogs',
    'action' => 'view',
);
$routesArray[] = array(
    'pattern' => '~^edison~',
    'controller' => 'page',
    'action' => 'edison',
);
$routesArray[] = array(
    'pattern' => '~^specialisms~',
    'controller' => 'page',
    'action' => 'specialisms',
);
$routesArray[] = array(
    'pattern' => '~^c-suite~',
    'controller' => 'page',
    'action' => 'c-suite',
);
$routesArray[] = array(
    'pattern' => '~^tactical-solutions~',
    'controller' => 'page',
    'action' => 'tactical_solutions',
);
$routesArray[] = array(
    'pattern' => '~^operational-solutions~',
    'controller' => 'page',
    'action' => 'operational_solutions',
);
$routesArray[] = array(
    'pattern' => '~^services~',
    'controller' => 'page',
    'action' => 'services',
);
$routesArray[] = array(
    'pattern' => '~^tech-community~',
    'controller' => 'page',
    'action' => 'tech_community',
);
$routesArray[] = array(
    'pattern' => '~^seeking-talent~',
    'controller' => 'page',
    'action' => 'looking',
);
$routesArray[] = array(
    'pattern' => '~^terms-and-conditions~',
    'controller' => 'page',
    'action' => 'terms-and-conditions',
);
$routesArray[] = array(
    'pattern' => '~^privacy-policy~',
    'controller' => 'page',
    'action' => 'privacy-policy',
);
$routesArray[] = array(
    'pattern' => '~^contact-us~',
    'controller' => 'cv',
    'action' => 'contact_us',
);
$routesArray[] = array(
    'pattern' => '~^data-generator/([A-Za-z0-9\+\-\.\,_%]{1,150})~',
    'controller' => 'panel/data_generator',
    'action' => 'execute',
);
$routesArray[] = array(
    'pattern' => '~^form_tester/([A-Za-z0-9\+\-\.\,_%]{1,150})~',
    'controller' => 'panel/form_tester',
    'action' => 'send',
);

/* End of file */