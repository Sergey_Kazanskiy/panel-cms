$(document).ready(function($) {
        
//Слайдер на страничке Tech.html --------------------------------------------------------------------------------------------------------------
    $('.alerts-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        dots: false,
    });

//Слайдер на страничке  About_Us.html--------------------------------------------------------------------------------------------------------------
//     $('.un-slider').slick({
//         slidesToShow: 1,
//         slidesToScroll: 1,
//         arrows: true,
//         dots: true,
//     });

//Меню ------------------------------------------------------------------------------------
//     $(".menu-icon").click(function () {
//         $("#site").addClass('open-menu');
//     });
//
//     $(".close-menu").click(function () {
//         $("#site").removeClass('open-menu');
//     });


    $(".menu").accordion({
        accordion:true,
        speed: 500,
        closedSign: '',
        openedSign: ''
    });

// //Плавнаяч прокрутка--------------------------------------------------------------------
// var linkNav = document.querySelectorAll('[href^="#"]'),
//     V = 0.5;  // скорость, может иметь дробное значение через точку
// for (var i = 0; i < linkNav.length; i++) {
//   linkNav[i].addEventListener('click', function(e) {
//     e.preventDefault();
//     var w = window.pageYOffset,  // прокрутка
//         hash = this.href.replace(/[^#]*(.*)/, '$1');  // id элемента, к которому нужно перейти
//         t = document.querySelector(hash).getBoundingClientRect().top,  // отступ от окна браузера до id
//         start = null;
//     requestAnimationFrame(step);  // подробнее про функцию анимации [developer.mozilla.org]
//     function step(time) {
//       if (start === null) start = time;
//       var progress = time - start,
//           r = (t < 0 ? Math.max(w - progress/V, w + t) : Math.min(w + progress/V, w + t));
//       window.scrollTo(0,r);
//       if (r != w + t) {
//         requestAnimationFrame(step)
//       } else {
//         location.hash = hash  // URL с хэшем
//       }
//     }
//   }, false);
// }

// закладки на страничке Specialisms.html --------------------------------------------------------------------------------------------------------------

    // $('.sb-menu').on('click', 'li:not(.active)', function() {
    //     $(this).addClass('active').siblings().removeClass('active').parents('.specialist-block').find('.specialist-box').eq($(this).index()).fadeIn(150).siblings('.specialist-box').hide();
    // })
    // $(".sb-menu li").eq(0).click();

// Слайдер на главной страничке (первый экран) --------------------------------------------------------------------------------------------------------------
//     //Большой слайдер
//     $('.head-slider').slick({
//         dots: true,
//         fade: true,
//         arrows: false,
//         autoplay: true
//     });
//
//     $(".hc-slider-pic").each(function(){
//         var src = $(this).find('.hc-slider-img img').attr('src');
//         //alert(src );
//         $(this).css('backgroundImage', "url(" + src + ")");
//     });

// Слайдер на главной страничке --------------------------------------------------------------------------------------------------------------
//     $('.roles-slider').slick({
//         slidesToShow: 3,
//         slidesToScroll: 1,
//         arrows: true,
//         dots: false,
//         responsive: [
//             {
//                 breakpoint: 1024,
//                 settings: {
//                     slidesToShow: 2,
//                 }
//             },
//             {
//                 breakpoint: 650,
//                 settings: {
//                     slidesToShow: 1,
//                 }
//             }
//         ]
//     });

// Всплывалка --------------------------------------------------------------------------------------------------------------
//     $(".btn-open-popup").click(function(e){
//         e.preventDefault();
//         $("#site").addClass('popup-open');
//     });
//
//     $(".popup-fon, .close-popup").click(function(){
//         $("#site").removeClass('popup-open');
//     });

// закладки на страничке Edison.html--------------------------------------------------------------------------------------------------------------
    // закладки
    $('.ed-bookmark').on('click', 'li:not(.active)', function() {
        $(this).addClass('active').siblings().removeClass('active').parents('.ed-tabs').find('.ed-bookmarker-box').eq($(this).index()).fadeIn(150).siblings('.ed-bookmarker-box').hide();
    })
    $(".ed-bookmark li").eq(0).click();

//Замена стандартного селекта --------------------------------------------------------------------------------------------------------------
//     $( ".select" ).selectmenu();

// Добавление поля для ссылки Linked  на страничке Contact.html --------------------------------------------------------------------------------------------------------------
    $(".cf-linked-open").click(function () {
        $(".cf-linked").show(100);
    });

//Анимация при скроле--------------------------------------------------------------------------------------------------------------
    AOS.init({
        useClassNames: true,
        initClassName: false,
        animatedClassName: 'animated',
        once: true,
    });

// Анимация шапки----------------------------------------------------------------------------------------------------------------
    $(window).scroll(function(){
        if ($(this).scrollTop() > 20) {
            $('.header').addClass('scroll-active');
        } else {
            $('.header').removeClass('scroll-active');
        }
    });
});//конец ready