<?php

class PageController extends Controller
{
    use Validator;

    public function indexAction()
    {
        Model::import('panel/videos');
        $this->view->video = VideosModel::getVideoByName('home-what-we-do');
        $this->view->video_home_tech = VideosModel::getVideoByName('home-tech-community');

        Request::setTitle('Home');
        Request::setKeywords('');
        Request::setDescription('');
    }


    public function testAction()
    {
        Request::setTitle('Test');
    }


    public function edisonAction()
    {
        Request::setTitle('Edison');
    }


    public function specialismsAction()
    {
        $this->view->list = PageModel::getSectors();

        Request::setTitle('Specialisms');
    }


    public function c_suiteAction()
    {
        Request::setTitle('C-suite Advisory');
    }


    public function tactical_solutionsAction()
    {
        Model::import('panel/videos');
        $this->view->video = VideosModel::getVideoByName('tactical-solutions');

        Request::setTitle('Tactical solutions for building teams');
    }


    public function operational_solutionsAction()
    {
        Request::setTitle('Operational solutions for scaling businesses');
    }


    public function servicesAction()
    {
        Model::import('panel/videos');
        $this->view->video = VideosModel::getVideoByName('services-video');

        Request::setTitle('Candidate Services');
    }


    public function tech_communityAction()
    {
        Model::import('panel/event_card');
        $this->view->events = Event_cardModel::getAll();

        Model::import('panel/videos');
        $this->view->video = VideosModel::getVideoByName('tech-community');

        Request::setTitle('Tech Community');
    }


    public function lookingAction()
    {
        Model::import('panel/videos');
        $this->view->video_home_tech = VideosModel::getVideoByName('home-tech-community');
        Request::setTitle('Looking for Talent');
    }

    public function loginAction()
    {
        if ($this->startValidation()) {
            $email = $this->validatePost('email',       'Email',    'required|trim|email');
            $pass  = $this->validatePost('password',    'Password', 'required|trim|min_length[6]|max_length[32]');

            if ($this->isValid()) {
                $user = PageModel::getUserByEmail($email);

                // Check password
                if ($user && $user->deleted == 'no' && $user->password == md5($pass)) {
                    User::setUserSession($user->id); // Create user session
                    redirectAny(get('url') ? get('url') : url('profile'));
                } else
                    Request::returnError('Invalid email and/or password. Please check your data and try again');
            } else {
                if (Request::isAjax())
                    Request::returnErrors($this->validationErrors);
            }
        }

        Request::setTitle('Login / Register');
    }

    public function registerAction()
    {
        Request::ajaxPart();

        if ($this->startValidation()) {
            $this->validatePost('firstname',     'First Name',        'required|trim|min_length[1]|max_length[100]');
            $this->validatePost('lastname',      'Last Name',         'required|trim|min_length[1]|max_length[100]');
            $this->validatePost('email',         'Email',             'required|trim|min_length[1]|max_length[100]|valid_email');
            $this->validatePost('tel',           'Telephone Number',  'trim|min_length[0]|max_length[100]');
//            $this->validatePost('job_title',     'Job Title',         'trim|min_length[0]|max_length[250]');
//            $this->validatePost('pay_type',      'Pay Type',          'trim|min_length[0]|max_length[250]');
//            $this->validatePost('salary_range',  'Salary Range',      'trim|min_length[0]|max_length[250]');
//            $this->validatePost('pay_rate',      'Pay Rate',          'trim|min_length[0]|max_length[250]');
//            $this->validatePost('current_salary','Current Salary',    'trim|min_length[0]|max_length[250]');
//            $this->validatePost('location',      'Current Location',  'trim|min_length[0]|max_length[250]');
            $this->validatePost('password',      'Password',          'required|trim|min_length[6]|max_length[32]');
            $this->validatePost('password2',     'Confirm Password',  'required|trim|min_length[6]|max_length[32]');
            $this->validatePost('check',         'Privacy Policy',    'required|trim|min_length[1]');

            $checkUser = PageModel::getUserByEmail(post('email'));
            if ($checkUser)
                $this->addError('email', 'This email already exist in system');

            if (post('password') !== post('password2'))
                $this->addError('password', 'Passwords should match');

            if ($this->isValid()) {
                $data = array(
                    'firstname'         => post('firstname'),
                    'lastname'          => post('lastname'),
                    'email'             => post('email'),
                    'tel'               => post('tel'),
//                    'job_title'         => post('job_title'),
//                    'pay_type'          => post('pay_type'),
//                    'salary_range'      => post('salary_range'),
//                    'pay_rate'          => post('pay_rate'),
//                    'current_salary'    => post('current_salary'),
//                    'location'          => post('location'),
                    'role'              => 'user',
                    'password'          => md5(post('password')),
                    'slug'              => makeSlug(post('firstname') . ' ' . post('lastname')),
                    'reg_time'          => time(),
                    'last_time'         => time(),
                );

                $result   = Model::insert('users', $data); // Insert row
                $insertID = Model::insertID();

                if (!$result && $insertID) {
                    User::setUserSession($insertID); // Create user session
                    Request::addResponse('redirect', false, url('profile'));
                } else {
                    Request::returnError('Database error');
                }
            } else {
                if (Request::isAjax())
                    Request::returnErrors($this->validationErrors);
            }
        }

        Request::setTitle('Login/Register');
    }


    public function terms_and_conditionsAction()
    {
        Model::import('panel/content_blocks');
        $this->view->terms_block = Content_blocksModel::getBlockByName('terms-and-conditions');

        Request::setTitle('Terms & Conditions');
    }


    public function privacy_policyAction()
    {
        Model::import('panel/content_blocks');
        $this->view->privacy_block = Content_blocksModel::getBlockByName('privacy-policy');

        Request::setTitle('Privacy Policy');
    }


    public function arrange_callAction()
    {
        Request::ajaxPart();


        if ($this->startValidation()) {
            $this->validatePost('name',             'Name',                 'required|trim|min_length[1]|max_length[200]');
            $this->validatePost('company',          'Company',              'trim|min_length[1]|max_length[100]');
            $this->validatePost('email',            'Email',                'trim|email');
            $this->validatePost('tel',              'Contact Number',       'required|trim|min_length[1]|max_length[100]');
            $this->validatePost('check',            'Checkbox',             'trim|min_length[1]');

            if ($this->isValid()) {
                $this->view->data = array(
                    'name'      => post('name'),
                    'company'   => post('company'),
                    'email'     => post('email'),
                    'tel'       => post('tel'),
                    'time'      => time()
                );

                // Send email to admin
                require_once(_SYSDIR_.'system/lib/phpmailer/class.phpmailer.php');
                $mail = new PHPMailer;

                // Mail to client/consultant
                $mail->IsHTML(true);
                $mail->SetFrom(NOREPLY_MAIL, NOREPLY_NAME);
                $mail->AddAddress(ADMIN_MAIL);
                //$mail->AddCC('');
                //if (isset($file['filename']))
                //    $mail->addAttachment($file['filepath'] . $file['filename']);

                $mail->Subject = 'New Call Booking';
                $mail->Body = $this->getView('modules/page/views/email_templates/book_call.php');
                $mail->AltBody = 'Note: Our emails are a lot nicer with HTML enabled!';
                $mail->Send();

                //$mail->ClearAllRecipients();
                //$mail->clearAttachments();

                Request::addResponse('html', '.callback_form', '<h3 class="title"><span>Thank you!</span></h3>');
                Request::endAjax();
            } else {
                if (Request::isAjax())
                    Request::returnErrors($this->validationErrors);
            }
        }

        Request::addResponse('html', '#popup', $this->getView());
    }


    public function refer_friendAction()
    {
        Request::ajaxPart();

        if ($this->startValidation()) {
            $this->validatePost('your_name',    'Your name',                    'required|trim|min_length[1]|max_length[100]');
            $this->validatePost('your_email',   'Your email',                   'required|trim|min_length[1]|max_length[60]|email');
            $this->validatePost('your_tel',     'Your telephone number',        'required|trim|min_length[1]|max_length[20]');
            $this->validatePost('friend_name',  'Your friend\'s name',            'required|trim|min_length[1]|max_length[100]');
            $this->validatePost('friend_email', 'Your friend\'s email',            'required|required|trim|min_length[1]|max_length[60]|email');
            $this->validatePost('friend_tel',   'Your friend\'s telephone number', 'required|trim|min_length[1]|max_length[20]');
            $this->validatePost('check',        'Privacy Policy',               'required|trim');

            if ($this->isValid()) {
                $this->view->data = array(
                    'your_name'    => post('your_name'),
                    'your_email'   => post('your_email'),
                    'your_tel'     => post('your_tel'),
                    'friend_name'  => post('friend_name'),
                    'friend_email' => post('friend_email'),
                    'friend_tel'   => post('friend_tel')
                );

                // Send email to admin
                require_once(_SYSDIR_.'system/lib/phpmailer/class.phpmailer.php');
                $mail = new PHPMailer;

                // Mail to client/consultant
                $mail->IsHTML(true);
                $mail->SetFrom(NOREPLY_MAIL, NOREPLY_NAME);
                $mail->AddAddress(ADMIN_MAIL);
                //$mail->AddCC('');
                //if (isset($file['filename']))
                //    $mail->addAttachment($file['filepath'] . $file['filename']);

                $mail->Subject = 'New Call Booking';
                $mail->Body = $this->getView('modules/page/views/email_templates/refer_friend.php');
                $mail->AltBody = 'Note: Our emails are a lot nicer with HTML enabled!';
                $mail->Send();

                $mail->ClearAllRecipients();
                //$mail->clearAttachments();

                // todo: add email sender
                Request::addResponse('html', '.refer_form', '<h3 class="title"><span>Thank you!</span></h3>');
                Request::endAjax();
            } else {
                if (Request::isAjax())
                    Request::returnErrors($this->validationErrors);
            }
        }

        Request::addResponse('html', '#popup', $this->getView());
    }


    public function upload_jobAction()
    {
        Request::ajaxPart();

        if ($this->startValidation()) {
            $this->validatePost('name',     'Your name',     'required|trim|min_length[1]|max_length[100]');
            $this->validatePost('email',    'Your email',    'required|trim|min_length[1]|max_length[60]|email');
            $this->validatePost('company',  'Company',       'required|trim|min_length[1]|max_length[150]');
            $this->validatePost('tel',      'Telephone',     'required|trim|min_length[1]|max_length[20]');
            $this->validatePost('cv_field', 'CV',            'required|trim');
            $this->validatePost('check',    'Privacy Policy','required|trim');

            if ($this->isValid()) {
                $this->view->data = array(
                    'name'     => post('name'),
                    'email'    => post('email'),
                    'company'  => post('company'),
                    'tel'      => post('tel'),
                    'cv_field' => post('cv_field')
                );

                // Send email to admin
                require_once(_SYSDIR_.'system/lib/phpmailer/class.phpmailer.php');
                $mail = new PHPMailer;

                // Mail to client/consultant
                $mail->IsHTML(true);
                $mail->SetFrom(NOREPLY_MAIL, NOREPLY_NAME);
                $mail->AddAddress(ADMIN_MAIL);
                //$mail->AddCC('');

                if (post('cv_field'))
                    $mail->addAttachment(_SYSDIR_ . 'data/tmp/' . post('cv_field'));

                $mail->Subject = 'New Uploaded Vacancy';
                $mail->Body = $this->getView('modules/page/views/email_templates/upload_job.php');
                $mail->AltBody = 'Note: Our emails are a lot nicer with HTML enabled!';
                $mail->Send();


                Request::addResponse('html', '.refer_job', '<h3 class="title"><span>Thank you!</span></h3>');
                Request::endAjax();
            } else {
                if (Request::isAjax())
                    Request::returnErrors($this->validationErrors);
            }
        }

        Request::addResponse('html', '#popup', $this->getView());
    }

    public function profileAction()
    {
        if ($this->startValidation()) {
            $this->validatePost('firstname',                'First Name',               'required|trim|min_length[1]|max_length[100]');
            $this->validatePost('lastname',                 'Last Name',                'required|trim|min_length[1]|max_length[100]');
            $this->validatePost('email',                    'Email',                    'required|trim|min_length[1]|max_length[100]|valid_email');
            $this->validatePost('tel',                      'Telephone',                'trim|min_length[0]|max_length[100]');
//            $this->validatePost('pay_type',                 'Pay Type',                 'trim|min_length[0]|max_length[250]');
//            $this->validatePost('salary_range',             'Salary Range',             'trim|min_length[0]|max_length[250]');
//            $this->validatePost('pay_rate',                 'Pay Rate',                 'trim|min_length[0]|max_length[250]');
//            $this->validatePost('current_salary',           'Current Salary',           'trim|min_length[0]|max_length[250]');
            $this->validatePost('location',                 'Current Location',         'trim|min_length[0]|max_length[250]');
            $this->validatePost('job_title',                'Current Job Title',        'trim|min_length[0]|max_length[100]');
//            $this->validatePost('desired_location',          'Desired Location',        'trim|min_length[0]|max_length[100]');
            $this->validatePost('employment_status',        'Employment Status',        'trim|min_length[0]|max_length[100]');
            $this->validatePost('interview_availability',   'Interview Availability',   'trim|min_length[0]|max_length[100]');
            $pass  = $this->validatePost('password',     'New Password',             'trim|min_length[6]|max_length[16]');
            $pass2 = $this->validatePost('password2',    'Confirm New Password',     'trim|min_length[6]|max_length[16]');

            if ($this->isValid()) {

                $locations = post('locations') ? post('locations') : [];
                $sectors = post('sectors') ? post('sectors') : [];
//                $specialisms = post('specialisms') ? post('specialisms') : [];

                $data = array(
                    'firstname'             => post('firstname'),
                    'lastname'              => post('lastname'),
                    'email'                 => post('email'),
                    'tel'                   => post('tel'),
                    'job_title'             => post('job_title'),
                    'location'              => post('location'),
//                    'pay_type'              => post('pay_type'),
//                    'salary_range'          => post('salary_range'),
//                    'pay_rate'              => post('pay_rate'),
//                    'current_salary'        => post('current_salary'),
//                    'desired_salary'        => post('desired_salary'),
//                    'desired_location'      => post('desired_location'),
                    'employment_status'     => post('employment_status'),
                    'interview_availability'=> post('interview_availability'),
                    'sectors'               => '|' . implode('||', $sectors) . '|',
                    'locations'             => '|' . implode('||', $locations) . '|',
//                    'specialisms'           => '|' . implode('||', $specialisms) . '|',
                    'cv'                    => post('cv'),
                );

                if (post('password')) {
                    if ($pass == $pass2) {
                        $data['password'] = md5($pass);
                    } else
                        Request::returnError('Passwords should match');
                }


//                print_data(post('locations'));

                // Copy and remove image
                if ($data['cv']) {
                    if (User::get('cv') !== $data['cv']) {
                        if (File::copy('data/tmp/' . $data['cv'], 'data/cvs/' . $data['cv'])) {
                            File::remove('data/cv/' . $this->view->user->cv);
                        } else
                            print_data(error_get_last());
                    }
                }

                $result = Model::update('users', $data, "`id` = '" . ( User::get('id') ) . "'"); // Update row
                Request::returnError('Profile saved!');
                Request::endAjax();

            } else {
                if (Request::isAjax())
                    Request::returnErrors($this->validationErrors);
            }
        }


        // for you
        Model::import('jobs');

        $sectorsNames = explode('||', trim(User::get('sectors'), '|'));
        $sectors = [];
        if (count(array_filter($sectorsNames)) < 1) {
            $sectors = false;
        } else {
//            foreach ($sectorsNames as $item) {
//                $sec = JobsModel::getSectorByName($item);
//                $sectors[] = $sec->id;
//            }
        }

        Model::import('panel/locations');
        $loc = LocationsModel::get(User::get('desired_location'));
        $this->view->currency = $loc->currency_s;

        $this->view->jobs = JobsModel::search(false,false, $sectors, User::get('desired_location'), false, 5);
        $this->view->sectors   = JobsModel::getSectors();
//        $this->view->locations = JobsModel::getLocationsWhere(" `position` = 0 AND `continent` != 0 ");
//        $this->view->specialisms = JobsModel::getSpecialisms();
//        $this->view->shortlist = JobsModel::getFavorites(User::get('id'));
//        $this->view->applies = JobsModel::getApplies(User::get('email'));

        Request::setTitle('My Account');
    }


    public function get_connectedAction()
    {
        Request::ajaxPart();

        if ($this->startValidation()) {
            $this->validatePost('email', 'Email', 'required|trim|email');

            if ($this->isValid()) {
                $data = array(
                    'email' => post('email'),
                    'time'  => time()
                );

                $result   = Model::insert('subscribers', $data); // Insert row
                $insertID = Model::insertID();

                Request::addResponse('html', '.subs_res', '<h3 class="title"><span>Thank you!</span></h3>');
                Request::endAjax();
            } else {
                if (Request::isAjax())
                    Request::returnErrors($this->validationErrors);
            }
        }
    }


    public function exitAction()
    {
        setSession('user', null);
        setSession('user_info', null);
        redirect('/');
    }
}
/* End of file */