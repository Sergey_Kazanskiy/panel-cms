<?php /* Popup::head('Login');  ?>

<div class="w300 m_auto">
    <form id="login" class="form">
        <div class="row f_email">
            <label class="title">Email</label>
            <input id="email" name="email" type="email" required>
            <span class="error_text"></span>
        </div>

        <div class="row f_password">
            <label class="title">Password</label>
            <input id="password" name="password" type="password" required>
            <span class="error_text"></span>
        </div>

        <input id="wh" name="wh" type="hidden" value="<?php echo 0; ?>">
        <script>
            $("#wh").val(winW() + "x" + winH());
        </script>

        <input class="btn_awe mt_12" type="submit" onclick="removeErrors(); load('page/go_login', 'form:#login', 'url:url'); return false;" value="Log In">
<!--        <div class="action">-->
<!--            <div class="">Forgot password?</div>-->
<!--            <div></div>-->
<!--        </div>-->

        <div class="google_auth flex-btw mt_16">
            <a class="pointer white" onclick="load('page/sign_in');" style="line-height: 40px;">Create account?</a>
            <div class="g-signin2" data-onsuccess="onSignIn"></div>
        </div>
    </form>
</div>

<script src="https://apis.google.com/js/platform.js" async defer></script>
<?php Popup::foot(); ?>
<?php Popup::closeListener(); ?>


<!--<h4 id="status">Reset password</h4>-->
<!--<div class="f_recovery_email">-->
<!--    <span class="modal__input__tittle">Email</span>-->
<!--    <input id="recovery_email" name="recovery_email" type="email" required>-->
<!--    <span class="error_text"></span>-->
<!--</div>-->
<!--<input onclick="removeErrors(); load('page/forgot_password', 'email#recovery_email'); return false;" type="submit" required value="Reset Password">-->
 */ ?>
<div class="side-img">
    <img src="<?= _SITEDIR_ ?>public/images/side-img.png" alt="">
</div>
<div class="head-block" >
    <header class="header">
        <div class="fixed">
            <div class="top-line">
                <div class="head-logo">
                    <a class="logo" href="{URL:/}"><img src="<?= _SITEDIR_ ?>public/images/logo.png" height="154" width="155"/></a>
                </div>
                <div class="tl-right">
                    <div class="language <?= Request::getParam('color_dropdown') ?>">
                        <div class="language__item"><span>English</span></div>
                        <ul class="language__list">
                            <li class="active">English</li>
                            <li>Deutsch</li>
                            <li>Español</li>
                            <li>Français</li>
                            <li>Italiano</li>
                            <li>Pусский</li>
                        </ul>
                    </div>
                    <div class="menu-icon">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
    </header>
</div>

<section class="section-login" data-aos="fade-up">
    <div class="fixed">
        <div class="sl__title">Log In / Register</div>
        <form class="first-search" id="form_login">
            <div class="first__inputs">
                <input type="text" name="email" placeholder="Email Address" class="first__input">
                <input type="password" name="password" placeholder="Password" class="first__input">
                <button type="submit" class="more-link" onclick="load('page/login', 'form:#form_login'); return false;">Log In </button>
            </div>
        </form>
        <?php /*
        <div style=" width:300px; display: flex;justify-content: space-between;">
            <a onclick="fb_login();return false;"><img src="<?= _SITEDIR_ ?>public/images/facebook.png" style="width: 150px;cursor: pointer;"></a>
            <div class="g-signin2" data-onsuccess="onSignIn"></div>
        </div>
        */ ?>
    </div>
</section>
<section class="section-register" data-aos="fade-up">
    <div class="fixed" >
        <div class="sr__title title">Create Your Free Account</div>
        <div class="text-block sr__text-block">
            <p>Take advantage of the benefits by creating your account.</p>
            <ul>
                <li><p>Weekly job alerts based on your chosen criteria.</p></li>
                <li><p>Ability to create your own profile & upload your CV.</p></li>
                <li><p>Fast track your applications using your saved profile details.</p></li>
                <li><p>Keep track of your previous application history.</p></li>
            </ul>
        </div>
        <form id="form_register" class="sr-search">
            <div class="sr__inputs">
                <input type="text" placeholder="First Name" name="firstname" class="sr__input">
                <input type="text" placeholder="Last Name" name="lastname" class="sr__input">
                <input type="text" placeholder="Email Address" name="email" class="sr__input">
                <input type="text" placeholder="Phone Number" name="tel" class="sr__input">
                <?php /*
                <input type="text" placeholder="Current Job Title" name="job_title" class="sr__input">

                <select name="currency"  class="sr__input">
                    <option value="">Currency</option>
                    <option value="£">pound £</option>
                    <option value="€">euro €</option>
                    <option value="$">dollar $</option>
                </select>

                <select name="pay_type"  class="sr__input">
                    <option value="">Pay Type</option>
                    <option value="Annually">Annually</option>
                    <option value="Monthly">Monthly</option>
                </select>
                <select name="pay_rate"  class="sr__input">
                    <option value="">Pay rate</option>
                    <option value="500">500</option>
                    <option value="600">600</option>
                    <option value="700">700</option>
                    <option value="800">800</option>
                    <option value="900">900</option>
                    <option value="1000">1000</option>
                </select>
                <select name="salary_range"  class="sr__input">
                    <option value="">Salary Range</option>
                    <option value="0">0 – 10,000</option>
                    <option value="10000">10,001 – 20,000</option>
                    <option value="20000">20,001 – 30,000</option>
                    <option value="30000">30,001 – 40,000</option>
                    <option value="40000">40,001 – 50,000</option>
                    <option value="50000">50,001 – 60,000</option>
                    <option value="60000">60,001 – 80,000</option>
                    <option value="80000">80,001 – 100,000</option>
                    <option value="100000">100,001 – 120,000</option>
                    <option value="120000">120,001 – 140,000</option>
                    <option value="140000">140,001 – 160,000</option>
                    <option value="160000">160,001 – 180,000</option>
                    <option value="180000">180,001 – 200,000</option>
                    <option value="200000">200,001 +</option>
                </select>
                <input type="text" placeholder="Current Salary" name="current_salary" class="sr__input">
                <input type="text" placeholder="Current Location" name="location" class="sr__input">
  */ ?>
                <input type="password" placeholder="Create Password" name="password" class="sr__input">
                <input type="password" placeholder="Confirm Password" name="password2" class="sr__input">
                <div class="value">
                    <label class="checked__label">
                        <input type="checkbox" class="value__check" name="check">
                        <span class="checkmark"></span>
                        <div class="value__name">I have read and agree to the Privacy Policy
                            including GDPR guidelines</div>
                    </label>
                </div>
                <button type="submit" class="more-link" onclick="load('page/register', 'form:#form_register'); return false;">Create Account</button>
            </div>

        </form>
    </div>
</section>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script>

    function onSignIn(googleUser) {
        var profile = googleUser.getBasicProfile();

        if (profile){
            //get data
            let data = {
                userid: profile.getId(),
                firstname: profile.getGivenName(),
                lastname: profile.getFamilyName(),
                email: profile.getEmail(),
            };

            $.ajax({
                type: 'post',
                url: 'google-login',
                data: data,
                success: function(msg) {
                },
                error: function(){}
            })
        }

    }


    function handle_fb_data(response){
        FB.api('/me?fields=email,first_name,last_name', function(response) {
            $.ajax({
                type: 'post',
                url: 'facebook-login',
                data: response,
                success: function(msg) {
                },
                error: function(){}
            })
        });
    }

    function fb_login(){
        FB.getLoginStatus(function(response) {
            if (response.authResponse) {
                handle_fb_data(response);
            } else {
                FB.login(function(response){
                    if (response.authResponse) {
                        handle_fb_data(response);
                    }
                });
            }
        });
    }


    window.fbAsyncInit = function() {
        FB.init({
            appId      : '700497073940877',
            cookie     : true,
            xfbml      : true,
            version    : 'v3.2'
        });

        FB.AppEvents.logPageView();

    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

</script>
</Script>



