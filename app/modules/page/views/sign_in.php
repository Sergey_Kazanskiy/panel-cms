<?php Popup::head('Join XeBase :)', 'register'); ?>

    <div class="register_wrap">
        <div class="left">
            <div class="img">
                <img src="<?= _SITEDIR_; ?>public/images/stack.jpg">
            </div>
            <h1 class="mt_16">Create custom stack</h1>
            <span>Choose tools in stack and send short link of it to your employer, partner, etc.</span>
        </div>

        <div class="right">
            <form id="reg" class="form">
                <div class="title">Registration:</div>


                <div class="row f_nickname">
                    <label class="title">Nickname</label>
                    <input id="nickname" name="nickname" type="text" required placeholder="">
                    <span class="error_text"></span>
                </div>

                <div class="row f_email">
                    <label class="title">Email</label>
                    <input id="email" name="email" type="email" required placeholder="">
                    <span class="error_text"></span>
                </div>

                <div class="row f_password">
                    <label class="title">Password</label>
                    <input id="password" name="password" type="password" required placeholder="">
                    <span class="error_text"></span>
                </div>

                <input class="mt_16" type="submit" onclick="removeErrors(); load('page/go_sign_in', 'form:#reg'); return false;" value="Sigh Up">
            </form>
            <div class="google_auth flex-around mt_12">
                <div class="col4" style="line-height: 40px; padding: 0 10px;">OR</div>
                <div class="g-signin2" data-onsuccess="onSignIn"></div>
            </div>
        </div>
    </div>

    <script src="https://apis.google.com/js/platform.js" async defer></script>
<?php Popup::foot(); ?>
<?php Popup::closeListener(); ?>