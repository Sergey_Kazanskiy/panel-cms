<div class="head-block" >
    <header class="header">
        <div class="fixed">
            <div class="top-line">
                <div class="head-logo">
                    <a class="logo" href="{URL:/}"><img src="<?= _SITEDIR_ ?>public/images/logo.png" height="154" width="155"/></a>
                </div>
                <div class="tl-right">
                    <div class="language <?= Request::getParam('color_dropdown') ?>">
                        <div class="language__item"><span>English</span></div>
                        <ul class="language__list">
                            <li class="active">English</li>
                            <li>Deutsch</li>
                            <li>Español</li>
                            <li>Français</li>
                            <li>Italiano</li>
                            <li>Pусский</li>
                        </ul>
                    </div>
                    <div class="menu-icon">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
    </header>

</div>

<section class="section-account">
    <div class="fixed">
        <div class="sa__title" data-aos="fade-up">My Account</div>
        <a onclick="load('page/exit'); return false;"  class="more-link sa__log-out" data-aos="fade-up">Log Out</a>
        <form id="form_profile" class="sa__form">
            <div class="form-sec" data-aos="fade-up">
                <div class="form-sec__title title" >Personal Information</div>
                <div class="form-sec__inputs" >
                    <label class="form-sec__input">
                        First Name
                        <input type="text" name="firstname" value="<?= User::get('firstname') ?>" >
                    </label>
                    <label class="form-sec__input">
                        Last Name
                        <input type="text" name="lastname" value="<?= User::get('lastname') ?>" >
                    </label >
                    <label class="form-sec__input">
                        Email Address
                        <input type="text" name="email" value="<?= User::get('email') ?>">
                    </label>
                    <label class="form-sec__input">
                        Phone Number
                        <input type="text" name="tel" value="<?= User::get('tel') ?>">
                    </label>
                </div>
            </div>
            <div class="form-sec" data-aos="fade-up">
                <div class="form-sec__title title">Employment Information</div>
                <div class="form-sec__inputs">
                    <label class="form-sec__input">
                        Current Location
                        <input type="text" name="location" value="<?= User::get('location') ?>">
                    </label>
                    <label class="form-sec__input">
                        Current Job Title
                        <input type="text" name="job_title" value="<?= User::get('job_title') ?>">
                    </label >
                    <label class="form-sec__input">
                        Current Employment Status
                        <input type="text" name="employment_status" value="<?= User::get('employment_status') ?>">
                    </label>
                    <label class="form-sec__input">
                        Interview Availability
                        <input type="text" name="interview_availability" value="<?= User::get('interview_availability') ?>">
                    </label>
                    <?php /*
                    <label class="form-sec__input">
                        Pay Type
                        <select name="pay_type"  class="sr__input">
                            <option value="">Pay Type</option>
                            <option value="Annually" <?= checkOptionValue(post('pay_type'),'Annually', User::get('pay_type'))?>>Annually</option>
                            <option value="Monthly" <?= checkOptionValue(post('pay_type'),'Monthly', User::get('pay_type'))?>>Monthly</option>
                        </select>
                    </label>
                    <label class="form-sec__input">
                        Salary Range
                        <select name="salary_range"  class="sr__input">
                            <option value="">Salary Range</option>
                            <option value="0" <?= checkOptionValue(post('salary_range'),'0', User::get('salary_range'))?>><?= $this->currency ?>0 – <?= $this->currency ?>10,000</option>
                            <option value="10000" <?= checkOptionValue(post('salary_range'),'10000', User::get('salary_range'))?>><?= $this->currency ?>10,001 – <?= $this->currency ?>20,000</option>
                            <option value="20000" <?= checkOptionValue(post('salary_range'),'20000', User::get('salary_range'))?>><?= $this->currency ?>20,001 – <?= $this->currency ?>30,000</option>
                            <option value="30000" <?= checkOptionValue(post('salary_range'),'30000', User::get('salary_range'))?>><?= $this->currency ?>30,001 – <?= $this->currency ?>40,000</option>
                            <option value="40000" <?= checkOptionValue(post('salary_range'),'40000', User::get('salary_range'))?>><?= $this->currency ?>40,001 – <?= $this->currency ?>50,000</option>
                            <option value="50000" <?= checkOptionValue(post('salary_range'),'50000', User::get('salary_range'))?>><?= $this->currency ?>50,001 – <?= $this->currency ?>60,000</option>
                            <option value="60000" <?= checkOptionValue(post('salary_range'),'60000', User::get('salary_range'))?>><?= $this->currency ?>60,001 – <?= $this->currency ?>80,000</option>
                            <option value="80000" <?= checkOptionValue(post('salary_range'),'80000', User::get('salary_range'))?>><?= $this->currency ?>80,001 – <?= $this->currency ?>100,000</option>
                            <option value="100000" <?= checkOptionValue(post('salary_range'),'100000', User::get('salary_range'))?>><?= $this->currency ?>100,001 – <?= $this->currency ?>120,000</option>
                            <option value="120000" <?= checkOptionValue(post('salary_range'),'120000', User::get('salary_range'))?>><?= $this->currency ?>120,001 – <?= $this->currency ?>140,000</option>
                            <option value="140000" <?= checkOptionValue(post('salary_range'),'140000', User::get('salary_range'))?>><?= $this->currency ?>140,001 – <?= $this->currency ?>160,000</option>
                            <option value="160000" <?= checkOptionValue(post('salary_range'),'160000', User::get('salary_range'))?>><?= $this->currency ?>160,001 – <?= $this->currency ?>180,000</option>
                            <option value="180000" <?= checkOptionValue(post('salary_range'),'180000', User::get('salary_range'))?>><?= $this->currency ?>180,001 – <?= $this->currency ?>200,000</option>
                            <option value="200000" <?= checkOptionValue(post('salary_range'),'200000', User::get('salary_range'))?>><?= $this->currency ?>200,001 +</option>
                        </select>
                    </label>
                    <label class="form-sec__input">
                        Current Salary
                        <input type="text" placeholder="Current Salary" name="current_salary" class="sr__input" value="<?= User::get('current_salary') ?>">
                    </label>
 */?>
                </div>
            </div>
            <div class="form-sec" data-aos="fade-up">
                <div class="form-sec__title title">Your CV</div>
                <div class="form-sec__inputs">
                    <?php if (User::get('cv')) { ?>
                        <div class="cv__link">
                            <a href="<?= _SITEDIR_ . 'data/cvs/' . User::get('cv'); ?>"
                               download="<?= makeSlug('CV_' . User::get('firstname') . ' ' . User::get('lastname')) . '.' . File::format(User::get('cv')); ?>">Download Current CV</a>
                        </div>
                    <?php } ?>
                    <div class="file-block">
                        <input type="file" name="cv_field" accept=".doc, .docx, .txt, .pdf, .fotd" onchange="initFile(this); load('cv/upload/', 'field=#cv', 'preview=#cv_name');">
                        <input type="hidden" name="cv" id="cv" value="<?= post('cv', false, User::get('cv')); ?>">
                        <span>Drag &amp; Drop Here</span>
                    </div>
                </div>
            </div>
            <div class="form-sec form-sec_pass" data-aos="fade-up">
                <div class="form-sec__title title">Change Password</div>
                <div class="form-sec__inputs">
                    <label class="form-sec__input">
                        New Password
                        <input type="password" name="password" >
                    </label>
                    <label class="form-sec__input">
                        Confirm New Password
                        <input type="password" name="password2">
                    </label >
                </div>
            </div>
            <div class="form-sec form-sec_alerts" data-aos="fade-up">
                <div class="form-sec__title title">Job Alerts</div>
                <div class="form-sec__inputs">
                    <div class="form-sec__input">
                        Desired Locations
                        <ul class="items">
                        <?php if (is_array($this->locations) && count($this->locations) > 0) foreach ($this->locations as $item) { ?>
                            <li class="value">
                                <label class="checked__label">
                                    <input type="checkbox" class="value__check" name="locations[]" value="<?= $item->name ?>"
                                        <?= checkCheckboxValue(post('locations'), $item->name, explode('||', trim(User::get('locations'), '|'))); ?>>
                                    <span class="checkmark"></span>
                                    <div class="value__name"><?= $item->name ?></div>
                                </label>
                            </li>
                        <?php } ?>
                        </ul>
                    </div>
                    <div class="form-sec__input">
                        Desired Industries
                        <div  class="dropdown-check-list dropdown-check-list-wide " >
                            <span class="anchor">Industries</span>
                            <ul class="items">
                                <?php if (is_array($this->sectors) && count($this->sectors) > 0) foreach ($this->sectors as $item) { ?>
                                    <li class="value">
                                        <label class="checked__label">
                                            <input type="checkbox" class="value__check" name="sectors[]" value="<?= $item->name ?>"
                                                <?= checkCheckboxValue(post('sectors'), $item->name, explode('||', trim(User::get('sectors'), '|'))); ?>>
                                            <span class="checkmark"></span>
                                            <div class="value__name"><?= $item->name ?></div>
                                        </label>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <?php /*
                    <div class="form-sec__input">
                        Desired Job Specialisms
                        <div  class="dropdown-check-list dropdown-check-list-wide " >
                            <span class="anchor"> Job Specialisms</span>
                            <ul class="items">
                                <?php if (is_array($this->specialisms) && count($this->specialisms) > 0) foreach ($this->specialisms as $item) { ?>
                                    <li class="value">
                                        <label class="checked__label">
                                            <input type="checkbox" class="value__check" name="specialisms[]" value="<?= $item->name ?>"
                                                <?= checkCheckboxValue(post('specialisms'), $item->name, explode('||', trim(User::get('specialisms'), '|'))); ?>>
                                            <span class="checkmark"></span>
                                            <div class="value__name"><?= $item->name ?></div>
                                        </label>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
 */?>
                    <?php /*
                    <div class="form-sec__input">
                        Desired Salary
                        <select name="desired_salary" id="desired_salary" class="sr__input"  required>
                            <option value="0" <?= checkOptionValue(post('desired_salary'),'0', User::get('desired_salary'))?>><?= $this->currency ?>0 – <?= $this->currency ?>10,000</option>
                            <option value="10000" <?= checkOptionValue(post('desired_salary'),'10000', User::get('desired_salary'))?>><?= $this->currency ?>10,001 – <?= $this->currency ?>20,000</option>
                            <option value="20000" <?= checkOptionValue(post('desired_salary'),'20000', User::get('desired_salary'))?>><?= $this->currency ?>20,001 – <?= $this->currency ?>30,000</option>
                            <option value="30000" <?= checkOptionValue(post('desired_salary'),'30000', User::get('desired_salary'))?>><?= $this->currency ?>30,001 – <?= $this->currency ?>40,000</option>
                            <option value="40000" <?= checkOptionValue(post('desired_salary'),'40000', User::get('desired_salary'))?>><?= $this->currency ?>40,001 – <?= $this->currency ?>50,000</option>
                            <option value="50000" <?= checkOptionValue(post('desired_salary'),'50000', User::get('desired_salary'))?>><?= $this->currency ?>50,001 – <?= $this->currency ?>60,000</option>
                            <option value="60000" <?= checkOptionValue(post('desired_salary'),'60000', User::get('desired_salary'))?>><?= $this->currency ?>60,001 – <?= $this->currency ?>80,000</option>
                            <option value="80000" <?= checkOptionValue(post('desired_salary'),'80000', User::get('desired_salary'))?>><?= $this->currency ?>80,001 – <?= $this->currency ?>100,000</option>
                            <option value="100000" <?= checkOptionValue(post('desired_salary'),'100000', User::get('desired_salary'))?>><?= $this->currency ?>100,001 – <?= $this->currency ?>120,000</option>
                            <option value="120000" <?= checkOptionValue(post('desired_salary'),'120000', User::get('desired_salary'))?>><?= $this->currency ?>120,001 – <?= $this->currency ?>140,000</option>
                            <option value="140000" <?= checkOptionValue(post('desired_salary'),'140000', User::get('desired_salary'))?>><?= $this->currency ?>140,001 – <?= $this->currency ?>160,000</option>
                            <option value="160000" <?= checkOptionValue(post('desired_salary'),'160000', User::get('desired_salary'))?>><?= $this->currency ?>160,001 – <?= $this->currency ?>180,000</option>
                            <option value="180000" <?= checkOptionValue(post('desired_salary'),'180000', User::get('desired_salary'))?>><?= $this->currency ?>180,001 – <?= $this->currency ?>200,000</option>
                            <option value="200000" <?= checkOptionValue(post('desired_salary'),'200000', User::get('desired_salary'))?>><?= $this->currency ?>200,001 +</option>
                        </select>
                    </div>
 */ ?>
                </div>
            </div>
            <?php if (is_array($this->applies) && count($this->applies) > 0){ ?>
                <div class="form-sec" data-aos="fade-up">
                    <div class="form-sec__title title" >Jobs I’ve Applied For</div>
                    <ul class="form-sec__list">
                        <?php foreach ($this->applies as $item){ ?>
                            <li class="job-card">
                                <div class="job-card__title"><?= $item->title ?></div>
                                <ul class="job-card__details">
                                    <li><?= implode(', ', array_map(function($c) {
                                            return $c->location_name;
                                        }, $item->locations)); ?></li>
                                </ul>
                                <a href="{URL:job/<?= $item->slug ?>}" class="job-card__link more-link">Read More</a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
            <?php if (is_array($this->shortlist) && count($this->shortlist) > 0){ ?>
                <div class="form-sec" data-aos="fade-up">
                    <div class="form-sec__title title">Shortlisted Jobs</div>
                    <ul class="form-sec__list">
                        <?php foreach ($this->shortlist as $item){ ?>
                            <li class="job-card">
                                <div class="job-card__title" style="display: flex; justify-content: space-between; align-items: center;">
                                    <div><?= $item->title ?></div>
                                    <div id="add_<?= $item->id ?>">
                                        <?php
                                        if (!User::get('id')) { ?>
                                            <a style="font-size: 20px; cursor:pointer;color: yellow;"
                                               onclick="load('login')">add to list</a>
                                        <?php } else {
                                            if (!$item->saved){ ?>
                                                <a  class="plus"
                                                    onclick="load('jobs/add_shortlist', 'user=<?= User::get('id')?>', 'job=<?= $item->id ?>')"></a>
                                                <?php
                                            } else { ?>
                                                <a class="trashcan"
                                                   onclick="load('jobs/add_shortlist', 'user=<?= User::get('id')?>', 'job=<?= $item->id ?>')"><i class="fas fa-trash"></i></a>
                                            <?php }
                                        }
                                        ?>
                                    </div>
                                </div>
                                <ul class="job-card__details">
                                    <li><?= implode(', ', array_map(function($c) {
                                            return $c->location_name;
                                        }, $item->locations)); ?></li>
                                </ul>
                                <a href="{URL:job/<?= $item->slug ?>}" class="job-card__link more-link">Read More</a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
            <button type="submit" class="more-link sa__update" onclick="load('page/profile', 'form:#form_profile'); return false;" data-aos="fade-up">Update Profile</button>
            <button onclick="if (confirm('Are you sure you wish to remove your account? This will remove your personal details from our database and cannot be undone.'))
                        load('page/delete-account')" class="more-link sa__delete" data-aos="fade-up">Delete my account</button>
        </form>
    </div>
</section>
<?php if (is_array($this->jobs) && count($this->jobs) > 0){ ?>
    <section class="related-slider-2 related-slider">
        <div class="fixed">
            <div class="rs__title title" data-aos="fade-up">Recommended Jobs For You</div>
            <div class="rs__slider-wrapper" data-aos="fade-up">
                <div class="rs-2__slider">
                    <?php foreach ($this->jobs as $item) { ?>
                        <div class="slide">
                            <div class="slide__title"><?= $item->title ?></div>
                            <div class="slide__descr">
                                <?= reFilter($item->content_short) ?>
                            </div>
                            <ul class="slide__details">
                                <li></li>
                                <li><?= implode(', ', array_map(function($c) {
                                        return $c->location_name;
                                    }, $item->locations)); ?></li>
                            </ul>
                            <a href="{URL:job/<?= $item->slug ?>}" class="more-link">Read More</a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
<?php } ?>
<script>
    $('.rs-2__slider').slick({
        infinite: true,
        slidesToShow: 3,
        arrows: true,
        dots: false,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 1400,
                settings: {
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 1180,
                settings: {
                    slidesToShow: 1
                }
            },
        ]
    });
</script>


