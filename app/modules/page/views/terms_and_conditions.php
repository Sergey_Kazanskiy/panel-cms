<section id="explore" class="section_6">
    <div class="fixed pry_bl">
        <h3 class="title-big"><?= $this->terms_block->title; ?></h3>


        <div class="do-flex">
            <div class="df-text" style="max-width: 100%;">
                <div>
                    <!--<h3 class="df-title"></h3>-->
                    <p><?= reFilter($this->terms_block->content); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>