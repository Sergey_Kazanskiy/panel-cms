<?php

class About_usController extends Controller
{
    use Validator;

    public function indexAction()
    {
        Model::import('panel/videos');
        $this->view->video = VideosModel::getVideoByName('what-we-do');

        Request::setTitle('What We Do');
        Request::setKeywords('');
        Request::setDescription('');
    }

    public function profileAction()
    {
        $slug = Request::getUri(0);
        $this->view->profile = About_usModel::getUser($slug);

        if (!$this->view->profile)
            redirect(url('about-us','our-people'));

        Model::import('panel/team');
        $this->view->fun_images = TeamModel::getUserImages($this->view->profile->id);

        Request::setTitle($this->view->profile->firstname . ' ' . $this->view->profile->lastname);
    }

    public function our_peopleAction()
    {
        $this->view->list = About_usModel::getUsers(" AND `id` >= '9' ORDER BY `sort` ASC"); // AND `id` <= '21'
        Request::setTitle('Our People');
    }

    public function work_for_usAction()
    {
        Model::import('panel/testimonials');
        $this->view->testimonials = TestimonialsModel::getAll();

        Model::import('panel/videos');
        $this->view->video = VideosModel::getVideoByName('work-for-us');

        Request::setTitle('Work For Us');
    }

    public function contactAction()
    {
        Request::ajaxPart();

        $slug = Request::getUri(0);
        $this->view->get = $consultant = About_usModel::getUser($slug);

        if (!$consultant)
            redirect(url('about-us','our-people'));

        if ($this->startValidation()) {
            $this->validatePost('name',    'Name',    'required|trim|min_length[1]|max_length[200]');
            $this->validatePost('email',   'Email',   'required|trim|email');
            $this->validatePost('message', 'Message', 'required|trim|min_length[1]|max_length[500]');

            if ($this->isValid()) {
                // Send email to admin
                require_once(_SYSDIR_.'system/lib/phpmailer/class.phpmailer.php');
                $mail = new PHPMailer;

                // Mail to client/consultant
                $mail->IsHTML(true);
                $mail->SetFrom(NOREPLY_MAIL, NOREPLY_NAME);
                $mail->AddAddress($consultant->email);

                $mail->Subject = 'Vacancy applied';
                $mail->Body = $this->getView('modules/about_us/views/email_templates/contact.php');
                $mail->AltBody = 'Note: Our emails are a lot nicer with HTML enabled!';
                $mail->Send();

                Request::addResponse('html', '#apply_form', '<h3 class="title-small">Thank you!</h3>');
                Request::endAjax();
            } else {
                if (Request::isAjax())
                    Request::returnErrors($this->validationErrors);
            }
        }

        Request::addResponse('html', '#popup', $this->getView());
    }
}
/* End of file */