<?php

class CvController extends Controller
{
    use Validator;

    public function indexAction()
    {

    }


    public function contact_usAction()
    {
        if ($this->startValidation()) {
            $this->validatePost('name',             'Name',                 'required|trim|min_length[1]|max_length[200]');
            $this->validatePost('email',            'Email',                'required|trim|email');
            $this->validatePost('tel',              'Contact Number',       'required|trim|min_length[1]|max_length[100]');
            $this->validatePost('message',          'Further information',  'required|trim|min_length[1]|max_length[100]');
            $this->validatePost('linkedin',         'LinkedIn',             'trim|min_length[0]|max_length[100]|url');
            $this->validatePost('job_spec',         'Job spec',             'trim|min_length[0]|max_length[100]');
            $this->validatePost('cv',               'CV',                   'trim|min_length[0]|max_length[100]');
            $this->validatePost('check',            'GDPR',                 'required|trim|min_length[1]');

            if ($this->isValid()) {
                $data = array(
                    'name'      => post('name'),
                    'email'     => post('email'),
                    'tel'       => post('tel'),
                    'message'   => post('message'),
                    'linkedin'  => post('linkedin'),
                    'job_spec'  => post('job_spec'),
                    'cv'        => post('cv'),
                    'time'      => time()
                );

                // Copy job spec
                if ($data['job_spec']) {
                    if (!File::copy('data/tmp/' . $data['job_spec'], 'data/spec/' . $data['job_spec']))
                        print_data(error_get_last());
                }

                // Copy CV
                if ($data['cv']) {
                    if (!File::copy('data/tmp/' . $data['cv'], 'data/cvs/' . $data['cv']))
                        print_data(error_get_last());
                }


                $result   = Model::insert('cv_library', $data); // Insert row
                $insertID = Model::insertID();

                if (!$result && $insertID) {
                    // Send email to admin
                    $this->view->uvid = $insertID;
                    $this->view->data = $data;

                    require_once(_SYSDIR_.'system/lib/phpmailer/class.phpmailer.php');
                    $mail = new PHPMailer;

                    Model::import('panel/analytics');
                    $select = AnalyticsModel::get('admin_mail');
                    if ($select->value)
                        $mail = $select->value;
                    else
                        $mail = ADMIN_MAIL;

                    // Mail to admin
                    $mail->IsHTML(true);
                    $mail->SetFrom(NOREPLY_MAIL, NOREPLY_NAME);
                    $mail->AddAddress($mail);

                    $mail->Subject = 'Submited Contact Us form';
                    $mail->Body = $this->getView('modules/cv/views/email_templates/contact_us.php');
                    $mail->AltBody = 'Note: Our emails are a lot nicer with HTML enabled!';
                    $mail->Send();


                    Request::addResponse('html', '#contact_form', '<h3 class="title-small">Thank you!</h3>');
                    Request::endAjax();
                } else {
                    Request::returnError('Database error');
                }
            } else {
                if (Request::isAjax())
                    Request::returnErrors($this->validationErrors);
            }
        }

        Request::setTitle('Contact Us');
    }


    public function upload_cvAction()
    {
        Request::ajaxPart();

        if ($this->startValidation()) {
            $this->validatePost('name',             'Name',                 'required|trim|min_length[1]|max_length[200]');
            $this->validatePost('email',            'Email',                'required|trim|email');
            $this->validatePost('tel',              'Contact Number',       'required|trim|min_length[1]|max_length[100]');
            $this->validatePost('linkedin',         'LinkedIn',             'trim|min_length[0]|max_length[100]|url');
            $this->validatePost('message',          'Further information',  'trim|min_length[1]|max_length[100]');
            $this->validatePost('cv_field',         'CV',                   'required|trim|min_length[0]|max_length[100]');
            $this->validatePost('check',            'Agree',                'required|trim|min_length[0]|max_length[100]');

            if ($this->isValid()) {
                $data = array(
                    'name'      => post('name'),
                    'email'     => post('email'),
                    'tel'       => post('tel'),
                    'linkedin'  => post('linkedin'),
                    'message'   => post('message'),
                    'cv'        => post('cv_field'),
                    'time'      => time()
                );

                // Copy CV
                if ($data['cv']) {
                    if (!File::copy('data/tmp/' . $data['cv'], 'data/cvs/' . $data['cv']))
                        print_data(error_get_last());
                }

                $result   = Model::insert('cv_library', $data); // Insert row
                $insertID = Model::insertID();

                if (!$result && $insertID) {
                    Request::addResponse('html', '#upload_cv_form', '<h3 class="title-small">Thank you!</h3>');
                    Request::endAjax();
                } else {
                    Request::returnError('Database error');
                }
            } else {
                if (Request::isAjax())
                    Request::returnErrors($this->validationErrors);
            }
        }

        Request::addResponse('html', '#popup', $this->getView());
    }


    /**
     * Upload CV files, etc.
     */
    public function uploadAction()
    {
        Request::ajaxPart(); // if not Ajax part load

        $name    = post('name', true); // file name, if not set - will be randomly
        $path    = post('path', true, 'tmp'); // path where file will be saved, default: 'tmp'
        $field   = post('field', true, '#image'); // field where to put file name after uploading
        $preview = post('preview', true, '#preview_file'); // field where to put file name after uploading

        $path = 'data/' . $path . '/';

        $result = null;
        foreach ($_FILES as $file) {
            $result = File::UploadCV($file, $path, $name);
            break;
        }

        $newFileName = $result['name'] . '.' . $result['format']; // randomized name

        Request::addResponse('val', $field, $newFileName);
        Request::addResponse('html', $preview, $result['fileName']);
    }

    public function uploadVideoAction()
    {
        Request::ajaxPart(); // if not Ajax part load

        $name    = post('name', true); // file name, if not set - will be randomly
        $path    = post('path', true, 'tmp'); // path where file will be saved, default: 'tmp'
        $field   = post('field', true, '#image'); // field where to put file name after uploading
        $preview = post('preview', true, '#preview_file'); // field where to put file name after uploading

        $path = 'data/' . $path . '/';

        $result = null;
        foreach ($_FILES as $file) {
            $result = File::UploadVideo($file, $path, $name);
            break;
        }

        $newFileName = $result['name'] . '.' . $result['format']; // randomized name

        Request::addResponse('val', $field, $newFileName);
        Request::addResponse('html', $preview, $result['fileName']);
    }
}
/* End of file */