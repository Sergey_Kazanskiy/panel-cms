<?php

class BlogsController extends Controller
{

    public function indexAction()
    {
//        Model::import('panel/sectors');
        Model::import('panel/blog/categories');

        // Pagination
        $count = Model::count('blog', '*', "`deleted` = 'no' AND `posted` = 'yes'");
        Pagination::calculate(get('page', 'int'), 10, $count);
        $this->view->blogs = BlogsModel::getAll(Pagination::$start, Pagination::$end);

        // Get sector list and make ID => NAME array
        $sectors = CategoriesModel::getAll();
        $sectorsArray = array();
        foreach ($sectors as $sector)
            $sectorsArray[$sector->id] = $sector->name;
        $this->view->sectors = $sectorsArray;

        Request::setTitle('Our Blog');
        Request::setKeywords('');
        Request::setDescription('');
    }

    public function viewAction()
    {
        $slug = Request::getUri(0);
        $this->view->blog = BlogsModel::getBySlug($slug);
        if (!$this->view->blog)
            redirect(url('blogs'));

        $this->view->prev = BlogsModel::getPrevBlog($this->view->blog->id);
        $this->view->next = BlogsModel::getNextBlog($this->view->blog->id);

        Request::setTitle('Blog - ' . $this->view->blog->meta_title);
        Request::setKeywords($this->view->blog->meta_keywords);
        Request::setDescription($this->view->blog->meta_desc);
    }

    public function our_blogsAction()
    {
        Request::ajaxPart();

        $this->view->blogs = BlogsModel::getAll(3);

        // Get sector list and make ID => NAME array
        Model::import('panel/blog/categories');
        $sectors = CategoriesModel::getAll();
        $sectorsArray = array();
        foreach ($sectors as $sector)
            $sectorsArray[$sector->id] = $sector->name;
        $this->view->sectors = $sectorsArray;

        Request::addResponse('html', '.blog-list', $this->getView());
    }
}

/* End of file */