<form id="form_box" method="post" enctype="multipart/form-data">
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <!-- Title ROW -->
            <div class="col-xl-12 col-lg-12 col-sm-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <h1 class="page_title">Robots.txt</h1>
                    </div>
                </div>
            </div>

            <!-- Content -->
            <div id="flFormsGrid" class="col-lg-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="form-group">
                        <label for="content">
                            Content
                        </label>
                        <textarea class="form-control" type="text" name="text" id="text">
                            <?= reFilter($this->content) ?>
                        </textarea>
                    </div>
                </div>
            </div>

            <!-- Save Buttons -->
            <div class="col-xl-12 col-lg-12 col-sm-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div>
                            <a class="btn btn-success" onclick="load('panel/robots', 'form:#form_box'); return false;"><i class="fas fa-save"></i>Save Changes</a>
                            <a href="<?= url('robots.txt') ?>" class="btn btn-outline-info" target="_blank">View robots.txt</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</form>