<!-- BEGIN PAGE LEVEL STYLES -->
<link rel="stylesheet" type="text/css" href="<?= _SITEDIR_; ?>plugins/table/datatable/datatables.css">
<link rel="stylesheet" type="text/css" href="<?= _SITEDIR_; ?>plugins/table/datatable/dt-global_style.css">
<!-- END PAGE LEVEL STYLES -->

<div class="layout-px-spacing">
    <div class="row layout-top-spacing">

        <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
            <div class="widget-content widget-content-area br-6">

                <!-- Head -->
                <div class="flex-btw flex-vc mob_fc">
                    <h1>Modules</h1>
                </div>

                <div class="table-responsive mb-4 mt-4" style="margin-top: -56px !important;">
                    <table id="modules_tb" class="table table-hover" style="width:100%">
                        <thead>
                        <tr>
                            <th class="max_w_60">ID</th>
                            <th>Name</th>
                            <th>Version</th>
                            <th>Time</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php if ($this->list->num_rows) { ?>
                            <?php while ($list = mysqli_fetch_object($this->list)) { ?>
                                <tr>
                                    <td><?= $list->id; ?></td>
                                    <td><?= $list->name; ?></td>
                                    <td><?= $list->version; ?></td>
                                    <td><?= date('Y-m-d', $list->time); ?></td>
                                    <td class="option__buttons">
                                        <a onclick="load('panel/modules_edit/<?= $list->id; ?>');" style="cursor: pointer;" class="bs-tooltip fa fa-pencil-alt" title="Edit"></a>
                                        <?php /*
                                        <a href="{URL:panel/modules/delete/<?= $list->id; ?>}" class="bs-tooltip" title="Delete">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash-2"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path><line x1="10" y1="11" x2="10" y2="17"></line><line x1="14" y1="11" x2="14" y2="17"></line></svg>
                                        </a>
                                        */ ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        <?php } else { ?>
                            <tr>
                                <td colspan="5" class="center">No records</td>
                            </tr>
                        <?php } ?>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>

    </div>
</div>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?= _SITEDIR_; ?>plugins/table/datatable/datatables.js"></script>
<script>
    $(function () {
        $('#modules_tb').DataTable({
            "oLanguage": {
                "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                "sSearchPlaceholder": "Search...",
                // "sLengthMenu": "Results :  _MENU_",
            },
            "stripeClasses": [],
            // "lengthMenu": [10, 25, 50, 100],
            "pageLength": 25,
            "bPaginate": false,
            "bLengthChange": false,
            "bFilter": true,
            "bInfo": false,
            "bAutoWidth": false
        });
    });
</script>
<!-- END PAGE LEVEL SCRIPTS -->