<form id="form_box"  enctype="multipart/form-data">
    <div class="layout-px-spacing">
        <div class="row layout-top-spacing">
            <!-- Title ROW -->
            <div class="col-xl-12 col-lg-12 col-sm-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <h1 class="page_title"><a href="{URL:panel/sitemap}">Site Map</a> » Edit</h1>
                    </div>
                </div>
            </div>

            <!-- General -->
            <div id="flFormsGrid" class="col-lg-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <h4>General</h4>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <div class="form-group">
                                <label for="table">Table</label>
                                <input class="form-control" type="text" name="table" id="table" value="<?= post('table', false, $this->item->table); ?>">
                            </div>
                            <div class="form-group">
                                <label for="where">Where</label>
                                <input class="form-control" type="text" name="where" id="where" value="<?= post('where', false, $this->item->where); ?>">
                            </div>
                            <div class="form-group">
                                <label for="url">Url</label>
                                <input class="form-control" type="text" name="url" id="url" value="<?= post('url', false, $this->item->url); ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Save Buttons -->
            <div class="col-xl-12 col-lg-12 col-sm-12 layout-spacing">
                <div class="statbox widget box box-shadow">
                    <div class="widget-header">
                        <div>
                            <a class="btn btn-success" onclick="
                                    load('panel/sitemap/edit/<?= $this->item->id; ?>', 'form:#form_box'); return false;">
                                <i class="fas fa-save"></i>Save Changes
                            </a>
                            <a class="btn btn-outline-warning" href="{URL:panel/sitemap}"><i class="fas fa-ban"></i>Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
