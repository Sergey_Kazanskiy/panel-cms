<div class="fixed">
    <div class="job-article">
        <div class="ja-cont">
            <h3 class="title"><?= $this->job->title; ?></h3>

            <div class="ja-data">
                <div><span>Location:</span> <?php echo implode(", ", array_map(function ($location) {
                        return $location->location_name;
                    }, $this->job->locations)); ?></div>
                <div><span>Date Posted:</span> <?= date("d/m/Y", $this->job->time); ?></div>
                <div><span>Salary:</span> <?= $this->job->salary_value; ?></div>
                <div><span>Expires:</span> <?= date("d/m/Y", $this->job->time_expire); ?></div>
                <div><span>Function:</span> <?php echo implode(", ", array_map(function ($function) {
                        return $function->function_name;
                    }, $this->job->functions)); ?></div>
                <div><span>Role Type:</span> <?= ucfirst($this->job->contract_type); ?></div>
                <div><span>Package:</span> <?= $this->job->package; ?></div>
                <div><span></span></div>
            </div>

            <style>
                .mod_h > h3 {
                    margin: 28px 0 12px 0;
                    font: 26px/140% 'BigCityGrotesquePro', sans-serif;
                    color: #64C2C8;
                }
            </style>

            <div class="mod_h">
                <?= reFilter($this->job->content); ?>
            </div>


            <div class="ja-btn-block">
                <a class="btn-yellow" onclick="load('jobs/apply_now/<?= $this->job->slug; ?>');">Apply now</a>
                <a class="btn-blue" href="{URL:jobs}">Back to job lists</a>
            </div>

            <div class="ja-share">
                Share this job
                <div class="social-block">
                    <a href="{URL:/}"><span class="icon-LinkedIn"></span></a>
                    <a href="{URL:/}"><span class="icon-Twitter"></span></a>
                    <a href="{URL:/}"><span class="icon-mail"></span></a>
                </div>
            </div>
        </div>

        <div class="ja-card">
            <div class="jac-pic"><img src="<?php echo _SITEDIR_; ?>data/users/<?= $this->consultant->image; ?>" height="283" width="307" alt=""/></div>
            <div class="jac-cont">
                <div class="jac-name"><span><?= $this->consultant->firstname . ' ' . $this->consultant->lastname; ?></span> <?php echo implode("<br>", array_map(function ($location) {
                        return $location->sector_name;
                    }, $this->job->sectors)); ?></div>
                <ul class="jac-link">
                    <li><a href="tel:<?= $this->consultant->tel; ?>"><span><?= $this->consultant->tel; ?></span></a></li>
                    <li><a href="mailto:<?= $this->consultant->email; ?>"><b>Email me here</b></a></li>
                </ul>
                <a class="btn-yellow" onclick="load('jobs/apply_now/<?= $this->job->slug; ?>');">Apply now</a>
            </div>
        </div>
    </div>
</div>