<?php

class JobsController extends Controller
{
    use Validator;

    public function indexAction()
    {
        $keywords = post('keywords');
        $type     = post('type');
        $sector   = post('sector');
        $location = post('location');

        $this->view->list      = JobsModel::search($keywords, $type, $sector, $location, false, 30);
        $this->view->sectors   = JobsModel::getSectors();
        $this->view->locations = JobsModel::getLocations();

        // Tech stack icons
        Model::import('panel/tech_stack');
        $this->view->tech_list = Tech_stackModel::getArrayWithID();

        Request::setTitle('Search Jobs');
        Request::setKeywords('');
        Request::setDescription('');
    }

    public function viewAction()
    {
        $slug = Request::getUri(0);
        $this->view->job = JobsModel::get($slug);

        if (!$this->view->job)
            redirect(url('jobs'));

        $data['views'] = '++';
        Model::update('vacancies', $data, "`id` = '" . $this->view->job->id . "'");

        $this->view->consultant = JobsModel::getUser($this->view->job->consultant_id);

        Request::setTitle('Job - ' . $this->view->job->title);
    }

    public function searchAction()
    {
        Request::ajaxPart();

        $keywords   = post('keywords');
        $type       = post('type');
        $sector     = post('sector');
        $location   = post('location');

        // Tech stack icons
        Model::import('panel/tech_stack');
        $this->view->tech_list = Tech_stackModel::getArrayWithID();

        $this->view->list = JobsModel::search($keywords, $type, $sector, $location, false, 30);

        Request::addResponse('html', '#search_results_box', $this->getView());
    }

    public function apply_nowAction()
    {
        Request::ajaxPart();

        $slug = Request::getUri(0);
        $this->view->job = JobsModel::get($slug);

        if (!$this->view->job)
            redirect(url('jobs'));

        if ($this->startValidation()) {
            $this->validatePost('name',             'Name',                 'required|trim|min_length[1]|max_length[200]');
            $this->validatePost('email',            'Email',                'required|trim|email');
            $this->validatePost('tel',              'Contact Number',       'required|trim|min_length[1]|max_length[100]');
            $this->validatePost('linkedin',         'LinkedIn',             'trim|min_length[0]|max_length[100]|url');
            $this->validatePost('message',          'Further information',  'trim|min_length[1]|max_length[100]');
            $this->validatePost('cv_field',         'CV',                   'required|trim|min_length[0]|max_length[100]');
            $this->validatePost('check',            'Agree',                'required|trim|min_length[0]|max_length[100]');

            if ($this->isValid()) {
                $data = array(
                    'vacancy_id' => $this->view->job->id,
                    'name'       => post('name'),
                    'email'      => post('email'),
                    'tel'        => post('tel'),
                    'linkedin'   => post('linkedin'),
                    'message'    => post('message'),
                    'cv'         => post('cv_field'),
                    'time'       => time()
                );

                // Copy CV
                if ($data['cv']) {
                    if (!File::copy('data/tmp/' . $data['cv'], 'data/cvs/' . $data['cv']))
                        print_data(error_get_last());
                }

                $consultant = JobsModel::getUser($this->view->job->consultant_id);
                $this->view->data = $data;

                // Send email to admin
                require_once(_SYSDIR_.'system/lib/phpmailer/class.phpmailer.php');
                $mail = new PHPMailer;

                // Mail to client/consultant
                $mail->IsHTML(true);
                $mail->SetFrom(NOREPLY_MAIL, NOREPLY_NAME);
                $mail->AddAddress($consultant->email); //ADMIN_MAIL
//                $mail->AddAddress('tom@boldidentities.com'); //ADMIN_MAIL
//                $mail->AddAddress('shloserb@gmail.com'); //ADMIN_MAIL

                $mail->Subject = 'Vacancy Application';
                $mail->Body = $this->getView('modules/jobs/views/email_templates/apply_now.php');
                $mail->AltBody = 'Note: Our emails are a lot nicer with HTML enabled!';

                $mail->AddAttachment(_SYSDIR_ . 'data/cvs/' . $data['cv'], 'CV.' . File::format($data['cv']));

                $sendStatus = false;
                if ($mail->Send())
                    $sendStatus = true;

                $result   = Model::insert('cv_library', $data); // Insert row
                $insertID = Model::insertID();

                if (!$result && $insertID) {
                    Request::addResponse('html', '#apply_form', '<h3 class="title-small">Thank you! ' . ($sendStatus ? '' : '-') . '</h3>');
                    Request::endAjax();
                } else {
                    Request::returnError('Database error');
                }
            } else {
                if (Request::isAjax())
                    Request::returnErrors($this->validationErrors);
            }
        }

        Request::addResponse('html', '#popup', $this->getView());
    }

    public function job_alertsAction()
    {
        Request::ajaxPart();

        Model::import('panel/locations');
        Model::import('panel/sectors');

        $this->view->locations = LocationsModel::getAll();
        $this->view->sectors = SectorsModel::getAll();

        if ($this->startValidation()) {
            $this->validatePost('name',             'Name',                 'required|trim|min_length[1]|max_length[200]');
            $this->validatePost('email',            'Email',                'required|trim|email');
            $this->validatePost('tel',              'Contact Number',       'required|trim|min_length[1]|max_length[100]');
            $this->validatePost('location_ids',     'Locations',            'required|is_array');
            $this->validatePost('sector_ids',       'Sectors',              'required|is_array');
            $this->validatePost('check',            'Agree',                'required|trim|min_length[0]|max_length[100]');

            if ($this->isValid()) {
                $data = array(
                    'name'       => post('name'),
                    'email'      => post('email'),
                    'tel'        => post('tel'),
                    'location'   =>  '|' . implode('||',post('location_ids')) . '|',
                    'sectors'    =>  '|' . implode('||',post('sector_ids')) . '|',
                    'time'       => time()
                );

                $this->view->locationsForAlerts = [];
                $this->view->sectorsForAlerts = [];

                foreach (post('location_ids') as $id)
                {
                    $location = LocationsModel::get($id);
                    $this->view->locationsForAlerts[] = $location->name;
                }

                foreach (post('sector_ids') as $id)
                {
                    $sector = SectorsModel::get($id);
                    $this->view->sectorsForAlerts[] = $sector->name;
                }

                // Send email to admin
                require_once(_SYSDIR_.'system/lib/phpmailer/class.phpmailer.php');
                $mail = new PHPMailer;

                // Mail to client/consultant
                $mail->IsHTML(true);
                $mail->SetFrom(NOREPLY_MAIL, NOREPLY_NAME);
                $mail->AddAddress(Request::getParam('admin_mail')->value);
//                $mail->AddAddress('tom@boldidentities.com'); //ADMIN_MAIL
//                $mail->AddAddress('shloserb@gmail.com'); //ADMIN_MAIL

                $mail->Subject = 'Job Alerts';
                $mail->Body = $this->getView('modules/jobs/views/email_templates/job_alerts.php');
                $mail->AltBody = 'Note: Our emails are a lot nicer with HTML enabled!';

                $mail->Send();


                $result   = Model::insert('subscribers', $data); // Insert row
                $insertID = Model::insertID();

                if (!$result && $insertID) {
                    Request::addResponse('html', '#apply_form', '<h3 class="title-small">Thank you! </h3>');
                    Request::endAjax();
                } else {
                    Request::returnError('Database error');
                }
            } else {
                if (Request::isAjax())
                    Request::returnErrors($this->validationErrors);
            }
        }

        Request::addResponse('html', '#popup', $this->getView());
    }

    public function add_shortlistAction()
    {
        Request::ajaxPart();

        Model::import('panel/vacancies');
        $favorite = VacanciesModel::getFavorite(User::get('id'), post('job'));

        if (!$favorite) {
            $data = [
                'user_id' => User::get('id'),
                'job_id'  => post('job'),
                'time'    => time(),
            ];

            Model::insert('shortlist_jobs', $data);

            if (post('type') == 'view') {
                Request::addResponse('html', '#shortlist', "<a  
                  onclick='load(`jobs/add_shortlist`, `user=" . User::get('id') . "`, `job=" . post('job') . "`)'>Remove from Shortlist</a>");
            } else {
                Request::addResponse('html', '#add_' . post('job'), "<a class='trashcan'  
                  onclick='load(`jobs/add_shortlist`, `user=" . User::get('id') . "`, `job=" . post('job') . "`)'><i class='fas fa-trash'></i></a>");
            }
        } else {
            Model::delete('shortlist_jobs', "`user_id` = '" . User::get('id') . "' AND `job_id` = '" . post('job') . "'");

            if (post('type') == 'view') {
                Request::addResponse('html', '#shortlist', "<a  
                  onclick='load(`jobs/add_shortlist`, `user=" . User::get('id') . "`, `job=" . post('job') . "`)'>Shortlist Job</a>");
            } else {
                Request::addResponse('html', '#add_' . post('job'), "<a class='plus'  
                  onclick='load(`jobs/add_shortlist`, `user=" . User::get('id') . "`, `job=" . post('job') . "`)'></a>");
            }

        }


    }

    public function latest_rolesAction()
    {
        Request::ajaxPart();

        // Tech stack icons
        Model::import('panel/tech_stack');
        $this->view->tech_list = Tech_stackModel::getArrayWithID();

        $this->view->list = JobsModel::search(false, false, false, false, 'DESC', 9);

        Request::addResponse('html', '.roles-slider', $this->getView());
        Request::addResponse('func', 'latestRolesSlider', '.roles-slider');
    }
}
/* End of file */